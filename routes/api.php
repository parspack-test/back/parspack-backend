<?php

use App\Http\Controllers\Api\V1\Process\ProcessController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('v1/register', [\App\Http\Controllers\Api\V1\Auth\AuthController::class, 'register']);
Route::post('v1/login', [\App\Http\Controllers\Api\V1\Auth\AuthController::class, 'login']);
Route::middleware('auth:api')->group(function () {
    Route::get('v1/logout', [\App\Http\Controllers\Api\V1\Auth\AuthController::class, 'logout']);
    Route::get('process', [ProcessController::class, 'index']);
    Route::post('file', [\App\Http\Controllers\Api\V1\File\FileController::class, 'store']);
    Route::get('file', [\App\Http\Controllers\Api\V1\File\FileController::class, 'index']);
    Route::post('directory', [\App\Http\Controllers\Api\V1\Directory\DirectoryController::class, 'store']);
    Route::get('directory', [\App\Http\Controllers\Api\V1\Directory\DirectoryController::class, 'index']);
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
});

