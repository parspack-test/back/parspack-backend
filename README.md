### Project with Laravel & Vue js.

In this  project you can see all process of system with Api Authentication

##### I hope you like it..

##### 1. Clone this repository (or download and extract the .zip)

##### 2. Run `composer install` from inside the project directory to download PHP dependencies.

##### 4. Run `php artisan artisan migrate` and just give the answers.

##### 4. Run `php artisan artisan passport:install` and just give the answers.

##### Postman Collection has been Attached.

##### Congratulations! You can now run this  project.
