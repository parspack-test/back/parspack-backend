<?php

namespace App\Http\Requests\Api\V1\File;

use Illuminate\Foundation\Http\FormRequest;

class FileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filename'=>'required|string|max:255',
            'extension'=>'required|string|min:1|max:10'
        ];
    }
}
