<?php

namespace App\Http\Controllers\Api\V1\Directory;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Directory\DirectoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Traits\Response;

class DirectoryController extends Controller
{
    use Response;

    public function store(DirectoryRequest $request)
    {
        $address = 'opt/myprogram/' . auth()->user()->name . '/' . $request->input('name');
        if (File::isDirectory($address)) {
            return $this->errorResponse('Directory.Exist');
        }
        if(File::makeDirectory($address, 0777, true, true)){
            return $this->successResponse('Directory.Created', $address);
        }
        $this->errorResponse('Directory.Created');
    }

    public function index()
    {
        $address = 'opt/myprogram/' . auth()->user()->name . '/';
        try {
            $directories = File::directories($address);
            return $this->successResponse('Directories.Index', $directories);
        } catch (\Exception $e) {
            return $this->errorResponse($e);
        }
    }
}
