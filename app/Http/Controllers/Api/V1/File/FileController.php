<?php

namespace App\Http\Controllers\Api\V1\File;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\File\FileRequest;
use Illuminate\Http\Request;
use App\Traits\Response;
use Illuminate\Support\Facades\File;

class FileController extends Controller
{
    use Response;

    public function store(FileRequest $request)
    {
        $address =
            'opt/myprogram/'
            . auth()->user()->name
            . '/' . $request->input('filename')
            . '.' . $request->input('extension');
        if (File::isFile($address)) {
            return $this->errorResponse('File.Exist');
        }
        try {
            File::put($address, '');
            return $this->successResponse('File.Create');
        } catch (\Exception $e) {
            return $this->errorResponse($e);
        }
    }

    public function index()
    {
        $address = 'opt/myprogram/' . auth()->user()->name . '/';
        try {
            $files = File::files($address);
            $result = [];
            foreach ($files as $file) {
                $result[] = $file->getPathname();
            }
            return $this->successResponse('File.Index',  $result);
        } catch (\Exception $e) {
            return $this->errorResponse($e);
        }
    }
}
