<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Auth\LoginRequest;
use App\Http\Requests\Api\V1\Auth\RegisterRequest;
use App\Models\User;
use App\Traits\CookieSetting;
use App\Traits\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use Response, CookieSetting;

    public function register(RegisterRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['password'] = Hash::make($validatedData['password']);
        $user = User::create($validatedData);
        if ($user) {
            $token = $user->createToken('User')->accessToken;
            $cookie = $this->getCookieDetails($token);
            return $this->successResponse('User.Created', [
                'user' => $user,
                'token' => $token
            ], 201)->cookie(
                $cookie['name'], $cookie['value'],
                $cookie['minutes'], $cookie['path'],
                $cookie['domain'], $cookie['secure'], $cookie['httpOnly']);
        }
        return $this->errorResponse('User.Created');
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->validated();
        if (Auth()->attempt($credentials)) {
            $user = Auth()->user();
            $token = $user->createToken('User')->accessToken;
            $cookie = $this->getCookieDetails($token);
            return $this->successResponse('User.LoggedIn', [
                'user' => Auth()->user(),
                'token' => $token,
            ])->cookie(
                $cookie['name'], $cookie['value'],
                $cookie['minutes'], $cookie['path'],
                $cookie['domain'], $cookie['secure'], $cookie['httpOnly']);
        }
        return $this->errorResponse('UserLoggedIn');
    }

    public function logout()
    {
        if (Auth()->user()->token('User')->revoke()) {
            setcookie('token', 'Expired', time() - 1, '/');
            return $this->successResponse('User.LoggedOut');
        }
        return $this->errorResponse('User.LoggedOut');
    }
}
