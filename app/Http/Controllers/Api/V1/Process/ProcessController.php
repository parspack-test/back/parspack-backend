<?php

namespace App\Http\Controllers\Api\V1\Process;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\Response;

class ProcessController extends Controller
{
    use Response;

    public function index()
    {
        $flag = Request()->get('flag');
        switch ($flag) {
            case '-A':
                $validatedFlag = '-A';
                break;
            case '-a':
                $validatedFlag = '-a';
                break;
            case '-d':
                $validatedFlag = '-d';
                break;
            case '-T':
                $validatedFlag = '-T';
                break;
            case '-r':
                $validatedFlag = '-r';
                break;
            case '-x':
                $validatedFlag = '-x';
                break;
            default:
                $validatedFlag = null;
        }
        $command = 'ps ' . $validatedFlag;
        exec($command, $output);
        $result = [];
        foreach ($output as $item) {
            $result[] = $item;
        }
        return $this->successResponse(200, $result);
    }
}
