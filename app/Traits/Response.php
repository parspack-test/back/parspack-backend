<?php


namespace App\Traits;


trait Response
{

    /**
     * json success response for api
     * @param $message
     * @param $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse($message, $data = null, $code = 200)
    {
        return response()
            ->json([
                'message' => __("messages.{$message}"),
                'data' => $data
            ], $code);
    }

    /**
     * json error response for api
     * @param $message
     * @param $error
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse($error, $code = 400)
    {
        return response()
            ->json([
                'message' => __("errors.{$error}"),
            ], $code);
    }
}
