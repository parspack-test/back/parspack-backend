<?php


namespace App\Traits;

use Illuminate\Support\Facades\Http;

trait CookieSetting
{
    public function getCookieDetails($value): array
    {
        return [
            'name' => 'token',
            'value' => $value,
            'minutes' => 1440,
            'path' => null,
            'domain' => null,
            // 'secure' => true, // for production
            'secure' => false, // for localhost
            'httpOnly' => true,
        ];
    }
}
